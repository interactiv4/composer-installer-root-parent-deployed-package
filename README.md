# Interactiv4 Composer Installer Root Parent Deployed Package - (Abadoned)

Description
-----------
**THIS MODULE HAS BEEN ABANDONED AND GROUPED IN THE FOLLOWING REPOSITORY:**

- (https://bitbucket.org/interactiv4/composer-installer)


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^7.2


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require interactiv4/composer-installer-root-parent-deployed-package --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/composer-installer-root-parent-deployed-package/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/composer-installer-root-parent-deployed-package/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2020 Interactiv4 S.L.