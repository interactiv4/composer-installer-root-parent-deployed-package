<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ComposerInstallerRootParentDeployedPackage;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Interactiv4\ComposerInstallerDeployedPackage\PackageTypesProvider;

/**
 * Class Plugin.
 *
 * @api
 */
class Plugin implements PluginInterface, EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public function activate(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function deactivate(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function uninstall(Composer $composer, IOInterface $io): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PackageEvents::PRE_PACKAGE_UPDATE => [
                ['registerPackageTypeWithPackageEvent', 10],
            ],
            PackageEvents::PRE_PACKAGE_INSTALL => [
                ['registerPackageTypeWithPackageEvent', 10],
            ],
            ScriptEvents::PRE_INSTALL_CMD => [
                ['registerPackageTypeWithScriptEvent', 10],
            ],
            ScriptEvents::PRE_UPDATE_CMD => [
                ['registerPackageTypeWithScriptEvent', 10],
            ],
        ];
    }

    /**
     * @param PackageEvent $event
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function registerPackageTypeWithPackageEvent(PackageEvent $event): void
    {
        $this->registerPackageType();
    }

    /**
     * @param Event $event
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function registerPackageTypeWithScriptEvent(Event $event): void
    {
        $this->registerPackageType();
    }

    /**
     * Register package type.
     */
    private function registerPackageType(): void
    {
        PackageTypesProvider::getInstance()->addPackageType('root-parent-deployed-package', './..');
    }
}
