<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\ComposerInstallerRootParentDeployedPackage\Test;

use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\Script\Event as ScriptEvent;
use Composer\Script\ScriptEvents;
use Interactiv4\ComposerInstallerDeployedPackage\PackageTypesProvider;
use Interactiv4\ComposerInstallerRootParentDeployedPackage\Plugin;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class PluginTest.
 *
 * @internal
 */
class PluginTest extends TestCase
{
    /**
     * @var PackageTypesProvider
     */
    private $packageTypesProvider;

    /**
     * @var Plugin
     */
    private $plugin;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        PackageTypesProvider::reset();
        $this->packageTypesProvider = PackageTypesProvider::getInstance();
        $this->plugin = new Plugin();
        parent::setUp();
    }

    /**
     * Test get subscribed events.
     */
    public function testGetSubscribedEvents(): void
    {
        static::assertSame(
            [
                PackageEvents::PRE_PACKAGE_UPDATE => [
                    ['registerPackageTypeWithPackageEvent', 10],
                ],
                PackageEvents::PRE_PACKAGE_INSTALL => [
                    ['registerPackageTypeWithPackageEvent', 10],
                ],
                ScriptEvents::PRE_INSTALL_CMD => [
                    ['registerPackageTypeWithScriptEvent', 10],
                ],
                ScriptEvents::PRE_UPDATE_CMD => [
                    ['registerPackageTypeWithScriptEvent', 10],
                ],
            ],
            $this->plugin::getSubscribedEvents()
        );
    }

    /**
     * Test register package type with package event.
     *
     * @param string $expectedPackageType
     * @param string $expectedDeployPath
     * @dataProvider registerPackageDataProvider
     */
    public function testRegisterPackageTypeWithPackageEvent(
        string $expectedPackageType,
        string $expectedDeployPath
    ): void {
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypes()
        );

        /** @var PackageEvent|MockObject $packageEvent */
        $packageEvent = $this->getMockBuilder(PackageEvent::class)->disableOriginalConstructor()->getMock();

        $packageEvent->expects(static::never())->method(static::anything());

        $this->plugin->registerPackageTypeWithPackageEvent($packageEvent);

        static::assertArrayHasKey(
            $expectedPackageType,
            $this->packageTypesProvider->getPackageTypesInfo()
        );

        static::assertSame(
            $expectedDeployPath,
            $this->packageTypesProvider->getPackageTypesInfo()[$expectedPackageType]
        );

        static::assertContains(
            $expectedPackageType,
            $this->packageTypesProvider->getPackageTypes()
        );

        static::assertContains(
            $expectedDeployPath,
            $this->packageTypesProvider->getDeployPaths()
        );
    }

    /**
     * Test register script type with script event.
     *
     * @param string $expectedPackageType
     * @param string $expectedDeployPath
     * @dataProvider registerPackageDataProvider
     */
    public function testRegisterScriptTypeWithScriptEvent(
        string $expectedPackageType,
        string $expectedDeployPath
    ): void {
        static::assertSame(
            [],
            $this->packageTypesProvider->getPackageTypes()
        );

        /** @var ScriptEvent|MockObject $scriptEvent */
        $scriptEvent = $this->getMockBuilder(ScriptEvent::class)->disableOriginalConstructor()->getMock();

        $scriptEvent->expects(static::never())->method(static::anything());

        $this->plugin->registerPackageTypeWithScriptEvent($scriptEvent);

        static::assertArrayHasKey(
            $expectedPackageType,
            $this->packageTypesProvider->getPackageTypesInfo()
        );

        static::assertSame(
            $expectedDeployPath,
            $this->packageTypesProvider->getPackageTypesInfo()[$expectedPackageType]
        );

        static::assertContains(
            $expectedPackageType,
            $this->packageTypesProvider->getPackageTypes()
        );

        static::assertContains(
            $expectedDeployPath,
            $this->packageTypesProvider->getDeployPaths()
        );
    }

    /**
     * @return array
     */
    public function registerPackageDataProvider(): array
    {
        return [
            ['root-parent-deployed-package', './..'],
        ];
    }
}
